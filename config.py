"""
config file for a versere grace app

see grace/config.py for available options to override here

note that config is loaded in this order:
- grace/config.py
- <appname>/config.py
"""

domains=["versere","localhost","127.0.0.1"]
port = '9003'


sitename='versere'
#meta_description="" #site description text for search engines
#meta_keywords="" #comma separated list of keywords for search engines

#permits={} #basis of permit system

attribution=True
default_class="Page"
default_page=1 #
urlpath=""  # no /evoke in url

show_words = True # give word count on each page header


from grace.data.schema import *  #for data definition

# include config.py files from class folders

#from evoke.config_base import Page



