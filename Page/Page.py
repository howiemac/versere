"""
override class for grace page

"""

import os, string, re

from calendar import month_name

from grace.render import html
from grace.lib import *
from grace.Page import Page as basePage
from grace.data import execute


class Page(basePage):

  def words(self):
     "override the grace version to give book totals"
     words= len(self.text.split())
     if self.parent==1793:
       chapters=0
       for p in self.get_children_by_kind("page"):
          words+= len(p.text.split())
          chapters+=1
       return f"{chapters} chapters {words}"
     return words

  def get_navbar_links(self):
    "override the grace version..."
    links=[("latest","/1/latest","latest")]
    for uid in (1066,442,2251,996,1793,2010,2121,1009,1767,1374):
      p=self.get(uid)
      links.append(
        (p.name,p.url(),p.name)
        )
#    links.append(("backlinks",self.get(7).url('fix_backlinks'),"update backlinks within this site"))
    return links

# disabled 6th December 2021 - no longer required?
#  def post(self,req):
#    """fix date and format when posting a draft to diary category 442
#       """
#    if (self.parent!=442): 
#      return basePage.post(self,req)
#    if basePage._posted(self,req):  
#      self.fix_diary_date(req)
##      self.reform_text(req)
#      if not req.error:
#       req.warning='diary date set'
#    return self.view(req)
#  post.permit='create page'

  def fix_diary_date(self,req):
    ""
    try:
     n=self.name.replace(',','')
     kind,day,month,year,place=n.split(' ',4)
#      print i.uid,':',day,' - ',month,' - ',year
     if day[1] in ['0','1','2','3','4','5','6','7','8','9']:
        day=day[:2]
     else:
        day=day[0]
     month=list(month_name).index(month)
     date='%s/%s/%s' % (day,month,year)
     self.when=DATE(date)
#     print "WHEN=>",self.when
     self.set_seq()
     self.flush()
    except:
     req.error="date handling problem - date not set for %s" % self.uid

  def fix_diary_dates(self,req):
    ""
    arts=self.list(parent=442)
    for i in arts:
     i.fix_diary_date(req) 
    req.message="diary dates updated"
    return self.get(1).view(req)

  def autopost(self,req):
    "autoposts oldest draft article"
    p=self.get_todays_article() 
    msg="no draft found" # default message
    if p: # if we have any draft
      p._posted(req) #post it
      msg= "%s posted on %s" % (p.uid,p.when) 
    return msg  
  autopost.permit="admin page"   

  @classmethod
  def get_todays_article(cls):
    """
    find the earliest draft article
    """
    articles=cls.list(stage='draft',orderby='uid',limit='0,1')
    return articles and articles[0] or None

  def fix_when(self,req):
    "custom date fixes"
    f = open("/home/howie/code/test/when.txt")
    t=f.read()
    out="'when' updated:"
    for l in t.splitlines():
      l=l.split('|')
      uid=l[1]
      when=l[2]
      out+= f"{uid} {when}<br>"
      p=self.get(uid)
      p.when=when
      p.flush()
    return out
#      print(l)
   
